package main

import (
	"test-proj/pkg/log"

	"go.uber.org/zap"
)

func main() {
	logger := zap.NewExample()
	defer logger.Sync()
	log.Info(logger, "Hello world")
}
