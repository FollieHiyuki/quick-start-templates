package log

import "go.uber.org/zap"

func Info(logger *zap.Logger, data string) {
	logger.Sugar().Info("This is a message: ", data)
}

func Plus(a int, b int) int {
	return a + b
}
