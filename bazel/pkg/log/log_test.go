package log

import "testing"

func TestPlus(t *testing.T) {
	got := Plus(2, 3)
	want := 5
	if want != got {
		t.Errorf("Expected '%d' but got '%d' instead", want, got)
	}
}
