{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs =
    { nixpkgs, ... }:
    let
      inherit (nixpkgs) lib;

      projects = {
        bazel = (
          pkgs: with pkgs; [
            bazel_8
            bazel-buildtools
          ]
        );
      };

      eachSystem =
        f:
        lib.genAttrs [
          "x86_64-linux"
          "aarch64-linux"
          "x86_64-darwin"
          "aarch64-darwin"
        ] (system: f nixpkgs.legacyPackages.${system});
    in
    {
      formatter = eachSystem (pkgs: pkgs.alejandra);

      templates = lib.genAttrs (builtins.attrNames projects) (name: {
        path = ./${name};
        description = "Quick-start template for a ${name} project.";
      });

      devShells = eachSystem (
        pkgs:
        builtins.mapAttrs (
          name: value:
          pkgs.mkShell {
            inherit name;

            meta.description = "Development shell for project template ${name}";
            packages = value pkgs;
          }
        ) projects
      );
    };
}
